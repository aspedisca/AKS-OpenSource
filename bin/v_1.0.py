#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import matplotlib
matplotlib.use("Qt4Agg")
import os
import sys
from functools import partial
import pickle as cPickle
import pickle
import rrdtool
from multiprocessing import Process
os.chdir("../")
sys.path.append('lib')
from a_ekle import *
from a_kaldir import *
from veri_dinle import *
from kontrol_ekrani import *
from grafik_olustur import *
from komut_gonder import *
from komut_olustur import *
os.chdir("bin")

class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.createActions()
        self.createMenus()
        QtGui.QApplication.processEvents()
        self.setWindowIcon(QtGui.QIcon(os.getcwd()+"/../data/icon/aspicon.png"))

        self.setWindowTitle("Akıllı Kontrol Sistemi")
        self.setMinimumSize(480,310)
        self.setMaximumSize(480,3000)
        self.resize(400,300)
        self.degis = True
        self.refresh()
        self.timer = QTimer()
        self.timer.timeout.connect(self.refresh)
        self.timer.start(600) #trigger every minute.

    def alan_ekrani(self,piname,hostname):
        alan_ekran(self,piname,hostname)              
    def refresh(self):
        #pilist = pickle.load( open( os.getcwd()+"/../data/veritabani.db", "rb"))
        datapi = open(os.getcwd()+"/../data/veritabani.db", 'rb')
        pilist=cPickle.load(datapi)
        datapi.close()      

        widget = QtGui.QWidget()                
        self.setCentralWidget(widget)
        vbox = QtGui.QGridLayout()
        baslik=QLabel()
        baslik.setText(" Kontrol Sistemi v1.0")
        font	 = QtGui.QFont("",20,QtGui.QFont.Bold,True)
        baslik.setStyleSheet('color: gray')
        baslik.setFont(font)
        baslik.setAlignment(Qt.AlignCenter)
        versiyon=QLabel()
        versiyon.setText("by DeathStar")
        versiyon.setAlignment(Qt.AlignBottom)
        vbox.addWidget(versiyon,0,1,1,3)
        vbox.addWidget(baslik,0,1,1,3) 

        konum={}
        buton={}
        led={}
        klima={}
        klima_bilgisi={}
        aydinlatma={}
        aydinlatma_bilgisi={}
        bosluk1=QLabel()
        bosluk2=QLabel()
        bosluk3=QLabel()
        bosluk4=QLabel()
        bosluk5=QLabel()
        satir_no=3
        led_kirmizi=QLabel()
        led_kirmizi.setPixmap(QPixmap(os.getcwd()+"/../data/icon/rd.png"))
        led_mavi=QLabel()
        led_mavi.setPixmap(QPixmap(os.getcwd()+"/../data/icon/blue.png"))
        led_yesil=QLabel()
        led_yesil.setPixmap(QPixmap(os.getcwd()+"/../data/icon/gr.png"))
        bilgi_kirmizi=QLabel()
        bilgi_kirmizi.setText("Acil Durum")
        bilgi_mavi=QLabel()
        bilgi_mavi.setText("Veri Alınamıyor")
        bilgi_yesil=QLabel()
        bilgi_yesil.setText("Kontrol Ediliyor")
        for piname,hostname in sorted(pilist.items()):
            if os.path.isfile(os.getcwd()+"/../data/"+hostname+".rrd"):   
                grid=QtGui.QGridLayout()
                #hbo[piname]=QtGui.QHBoxLayout()
                #vbo=QtGui.QVBoxLayout()
                #durum_piname=piname
                konum[piname]=QPushButton()
                buton[piname]=QPushButton()
                led[piname]=QLabel()
                ledler=QLabel()
                #ledler.setPixmap(QPixmap(os.getcwd()+"/../data/icon/ledler.png"))

                klima[piname]=QLabel()

                #klima_bilgisi[piname]=QLabel()
                aydinlatma[piname]=QLabel()
                #aydinlatma_bilgisi[piname]=QLabel()
                """Programınn veri alıp almadığı kontrol edilerek ana penceredeki ledlere bilgi verilecek eğer ana pencerede led kırmızı yanıp sönerse veri almadığını veya eksik veri aldığını yeşil yanıp sönerse doğru bir şekilde veri aldığını göstermiş olacak"""
                son_guncelleme=rrdtool.lastupdate(os.getcwd()+"/../data/"+hostname+".rrd")
                son_guncelleme_zaman=son_guncelleme["date"]
                anlik_zaman=datetime.datetime.now()
                #print(son_guncelleme_zaman)
                if (son_guncelleme_zaman+datetime.timedelta(minutes=2))<anlik_zaman and self.degis == True:
                    image="blue.png"
                elif(son_guncelleme_zaman+datetime.timedelta(minutes=2))<anlik_zaman and self.degis == False:
                    image="off.png"
                elif(son_guncelleme_zaman+datetime.timedelta(minutes=2))>anlik_zaman and self.degis == True:
                    veri_on_dakika=rrdtool.fetch(os.getcwd()+"/../data/"+hostname+".rrd","AVERAGE","-r","600","-s",str(int(time.time()-600)),"-e",str(int(time.time()-0)))
                    sensor_verileri=veri_on_dakika[2]
                    hava_kalitesi=sensor_verileri[0][2]
                    try:
                        if hava_kalitesi > 90:
                            image="rd.png"
                        else:
                            image="gr.png"
                    except:
                        image="gr.png"

                elif(son_guncelleme_zaman+datetime.timedelta(minutes=2))>anlik_zaman and self.degis == False:
                    image="off.png"
                else:
                    pass                 
                led[piname].setPixmap(QPixmap(os.getcwd()+"/../data/icon/"+image))
                buton[piname].setText(str(piname)+" Kontrol Sayfası")
                buton[piname].setGeometry(10,10,10,10)
                buton[piname].clicked.connect(partial(self.alan_ekrani,piname,hostname))
                #durum=mesaj_ver(piname)
                konum[piname].setText(str(piname))
                
                durumlar=komutlar(hostname)
                durumlar=durumlar.split(",")
                try :
                    klima_durum=durumlar[0]
                except :
                    klima_durum="bekleniyor"
                try :
                    aydinlatma_durum=durumlar[1]
                except :
                    aydinlatma_durum="bekleniyor"
                klima[piname].setText("Klima :"+str(klima_durum))
                aydinlatma[piname].setText("Aydınlatma : %"+str(aydinlatma_durum))
                vbox.addWidget(konum[piname],satir_no,0)
                vbox.addWidget(buton[piname],satir_no,1)
                vbox.addWidget(led[piname],satir_no,2)
                vbox.addWidget(klima[piname],satir_no,3)
                vbox.addWidget(aydinlatma[piname],satir_no,4)
                vbox.setAlignment(Qt.AlignTop)
                widget.setLayout(vbox)
                satir_no=satir_no+1
            else: 
                pass
        kayitli_alan_sayisi=QLabel()
        kayitli_alan_sayisi.setText(str(len(pilist))+" tane alan kayıtlı")
        vbox.addWidget(bosluk1,satir_no,0)
        vbox.addWidget(bosluk2,satir_no+1,0)
        vbox.addWidget(bosluk3,satir_no+2,0)
        vbox.addWidget(bosluk4,satir_no+3,0)
        #vbox.addWidget(bosluk5,satir_no+4,0)
        
        satir_no=satir_no+5
        vbox.addWidget(kayitli_alan_sayisi,satir_no+6,0)
        vbox.addWidget(led_yesil,satir_no+4,5)
        vbox.addWidget(bilgi_yesil,satir_no+4,4)
        vbox.addWidget(led_kirmizi,satir_no+5,5)
        vbox.addWidget(bilgi_kirmizi,satir_no+5,4)
        vbox.addWidget(led_mavi,satir_no+6,5)
        vbox.addWidget(bilgi_mavi,satir_no+6,4)
        #vbox.addWidget(ledler,satir_no+7,0,0,3)
        logo=QLabel()
        logo.setPixmap(QPixmap(os.getcwd()+"/../data/icon/3.png"))
        vbox.addWidget(logo,0,0)
        widget.setLayout(vbox)
        widget.show()
        if self.degis:
            self.degis = False
        else:
            self.degis = True
    
              
    def monitor(self):
        window.show()

    def about(self):     
        QtGui.QMessageBox.about(self, "Program Hakkında","Bu program kamu binalarında otomatik kontrol sistemini denetleyerek enerji verimliliğini arttırmak üzere hazırlanmıştır.")

    def aboutQt(self):    
        QtGui.QMessageBox.about(self, "AspireLab Hakkında","AspireLab Recep Tayyip Erdoğan Üniversitesi Mühendislik Fakültesinde bulunan Uygulamalı Sinyal İşleme ve Ensturmantasyon Labarotuvarı'dır.")
    def ekle(self): 
        self.dialog=alan_ekleme(self)
        self.dialog.show()


    def kaldir(self):
        self.dialog=alan_kaldirma(self)
        self.dialog.show()

    def createActions(self):
        self.monitorAct = QtGui.QAction("Kontrol Monitörü",self,shortcut=QtGui.QKeySequence.New,statusTip="Monitör Ekranını Göster", triggered=self.monitor)

        self.exitAct = QtGui.QAction("Çıkış", self, shortcut="Ctrl+Q",
                statusTip="Programdan çık", triggered=self.close)

        self.aboutAct = QtGui.QAction("Program Hakkında", self,
                statusTip="Program hakkında bilgi verir",
                triggered=self.about)

        self.aboutQtAct = QtGui.QAction("AspireLab Hakkında", self,
                statusTip="AspireLab hakkında bilgi verir",
                triggered=self.aboutQt)
        self.ekleAct = QtGui.QAction("Uç Birim Ekle", self,
                shortcut=QtGui.QKeySequence.New,
                statusTip="Uç birim ekle.", triggered=self.ekle)
        self.kaldirAct = QtGui.QAction("Uç Birim Kaldır", self,
                shortcut=QtGui.QKeySequence.New,
                statusTip="Uç birim kaldır.", triggered=self.kaldir)

    def createMenus(self):
        self.menumenu=self.menuBar().addMenu("Menü")
        self.menumenu.addSeparator()
        self.menumenu.addAction(self.monitorAct)
        self.menumenu.addAction(self.exitAct)
        
        self.editMenu = self.menuBar().addMenu("Kontrol Alanlarını Düzenle")
        self.editMenu.addAction(self.ekleAct)
        self.editMenu.addAction(self.kaldirAct)
        
        self.helpMenu = self.menuBar().addMenu("&Yardim")
        self.helpMenu.addAction(self.aboutAct)
        self.helpMenu.addAction(self.aboutQtAct)        



if __name__ == '__main__':
    """ilk önce subprocess yöntemiyle grafikleri oluşturacak program eş zamanlı olarak çalıştırılacak"""
    veri_akisi=Process(target=server)
    veri_akisi.start()
    komut_akisi=Process(target=kontrol_komut)
    komut_akisi.start()

    time.sleep(1)
    """veri tabanında kayıtlı dosya olup olmadığı kontrol ediliyor eğer  varsa yeni dosya açılmıyor ama yoksa yeni bir dosya açılıp içine liste kayıt edilecek.""" 
    import sys
    if os.path.isfile(os.getcwd()+"/../data/veritabani.db"):
        #print("File veritabani.db already exists")
        pass
    else:    
        pilist={}
        pickle.dump(pilist, open(os.getcwd()+"/../data/veritabani.db", "wb" ) )
        print("ok")
        datapi = open(os.getcwd()+"/../data/veritabani.db", 'wb')
        pickle.dump(pilist, datapi)
        datapi.close()

    """ana sayfa öğesi oluşturulup başlatılacak"""
    app = QtGui.QApplication(sys.argv)
    app.processEvents()
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
        
        
