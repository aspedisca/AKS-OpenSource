#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import rrdtool
from rrdtool import update as rrd_update
import time
import datetime
import os
def grafikOlustur():
    """while True:
        piids={'merkez1':1,'merkez2':2,'merkez3':3}
        piips={'pi1':1,'pi2':2,'pi3':3} 
        sicaklik=10
        isik=20
        havakalitesi=30
        nemorani=40
        for piid in sorted(piids.items()):
            dosyaAdi=str(piid)+".rrd"
            dosyaKonumu=os.getcwd()+"/../data/"+dosyaAdi
            if os.path.isfile(dosyaKonumu):
                print( dosyaAdi,"Icin veritabani daha once olusturulmus.")
            else:
                veritabani(dosyaKonumu)
        for piid in sorted(piids.items()):
            dosyaAdi=str(piid)+".rrd"
            dosyaKonumu=os.getcwd()+"/../data/"+dosyaAdi
            veritabaniGuncelle(dosyaKonumu,sicaklik,isik,havakalitesi,nemorani,dosyaAdi)
            grafikCiz(piid,dosyaKonumu)
    return True"""
def veritabani(dosyaKonumu):                  #sensorlerden alinan veriler 
                                              #icin rrd veritabani olusturuldu
                                              #her 5 dakikada bir sensorlerden veri al
                                              #1 gunde 5 dakika conuzurluk
                                              #1 haftada 15 dakika cozunurluk
                                              #1 ayda 1 saat cozunurluk
                                              #1 yilda 6 saat cozunurluk
    veritabani=rrdtool.create(dosyaKonumu,
    "--step","300","--start",'0',
    "DS:sicaklik:GAUGE:600:0:70",
    "DS:isik:GAUGE:600:0:15000",
    "DS:havakalitesi:GAUGE:600:0:500",
    "DS:nemorani:GAUGE:600:-5:105",
    "DS:dis_sicaklik:GAUGE:600:-20:60",
    "RRA:AVERAGE:0.5:1:288",
    "RRA:AVERAGE:0.5:6:672",
    "RRA:AVERAGE:0.5:24:744",
    "RRA:AVERAGE:0.5:288:1480",
    "RRA:MAX:0.5:1:288",
    "RRA:MAX:0.5:6:672",
    "RRA:MAX:0.5:24:744",
    "RRA:MAX:0.5:288:1480",
    "RRA:MIN:0.5:1:288",
    "RRA:MIN:0.5:6:672",
    "RRA:MIN:0.5:24:744",
    "RRA:MIN:0.5:288:1480")
    #print(dosyaKonumu, "Sensor degerleri icin veritabani olusturuldu.")
    return veritabani
def veritabaniGuncelle(dosyaKonumu,sicaklik,isik,havakalitesi,nemorani,dis_sicaklik,dosyaAdi):           #veritabanlarini guncelleyen fonksiyon
    rrd_update(dosyaKonumu,'N:%s:%s:%s:%s:%s'%(sicaklik,isik,havakalitesi,nemorani,dis_sicaklik));          #rrdtool kutuphanesindeki komut ile
    #print(dosyaAdi, "icin veritabanlari guncellendi.")
def grafikCiz(piid,dosyaKonumu):
    piid=str(piid)
    sonGuncelleme = rrdtool.last(dosyaKonumu)
    sonGuncellemedf = datetime.datetime.fromtimestamp(int(sonGuncelleme)).strftime('%Y-%m-%d %H\:%M\:%S')
    for z in ["daily","weekly","monthly","yearly"]:
        if z== "daily":
            period="d"
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"günlükSıcaklık.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Sicaklik(C)", "--height=300",
            "DEF:sicaklik="+dosyaKonumu+":sicaklik:AVERAGE",
            "DEF:dis_sicaklik="+dosyaKonumu+":dis_sicaklik:AVERAGE",
            "LINE1:sicaklik#00FF00:Oda Sicakligi",
            "LINE1:dis_sicaklik#FF0000:Dis Sicaklik",
            "COMMENT:\\n",
            "GPRINT:sicaklik:MIN:Min Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:sicaklik:MAX:Max Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",
            "GPRINT:dis_sicaklik:MIN:Min Dis Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:dis_sicaklik:MAX:Max Dis Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"günlükIşık_Şiddeti.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Isik Siddeti", "--height=300",
            "DEF:isik="+dosyaKonumu+":isik:AVERAGE",
            "LINE1:isik#FF0000:Isik Seviyesi",
            "GPRINT:isik:MIN:Min Isik\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:isik:MAX:Max Isik\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"günlükNem.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Nem(%)", "--height=300",
            "DEF:nemorani="+dosyaKonumu+":nemorani:AVERAGE",
            "LINE1:nemorani#000000:Nem Orani\\r",
            "GPRINT:nemorani:MIN:Min Nem Orani\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:nemorani:MAX:Max Nem Orani\: %6.2lf\\r",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"günlükHava_Kalitesi.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Hava Kalitesi", "--height=300",
            "DEF:havakalitesi="+dosyaKonumu+":havakalitesi:AVERAGE",
            "LINE1:havakalitesi#0000FF:Hava Kalitesi",
            "GPRINT:havakalitesi:MIN:Min Hava Kalitesi\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:havakalitesi:MAX:Max Hava Kalitesi\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
        if z== "weekly":
            period="w"
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"haftalikSıcaklık.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Sicaklik(C)", "--height=300",
            "DEF:sicaklik="+dosyaKonumu+":sicaklik:AVERAGE",
            "DEF:dis_sicaklik="+dosyaKonumu+":dis_sicaklik:AVERAGE",
            "LINE1:sicaklik#00FF00:Oda Sicakligi",
            "LINE1:dis_sicaklik#FF0000:Dis Sicaklik",
            "COMMENT:\\n",
            "GPRINT:sicaklik:MIN:Min Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:sicaklik:MAX:Max Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",
            "GPRINT:dis_sicaklik:MIN:Min Dis Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:dis_sicaklik:MAX:Max Dis Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",             
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"haftalikIşık_Şiddeti.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Isik Siddeti", "--height=300",
            "DEF:isik="+dosyaKonumu+":isik:AVERAGE",
            "LINE1:isik#FF0000:Isik Seviyesi",
            "GPRINT:isik:MIN:Min Isik\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:isik:MAX:Max Isik\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"haftalikNem.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Nem(%)", "--height=300",
            "DEF:nemorani="+dosyaKonumu+":nemorani:AVERAGE",
            "LINE1:nemorani#000000:Nem Orani\\r",
            "GPRINT:nemorani:MIN:Min Nem Orani\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:nemorani:MAX:Max Nem Orani\: %6.2lf\\r",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"haftalikHava_Kalitesi.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Hava Kalitesi", "--height=300",
            "DEF:havakalitesi="+dosyaKonumu+":havakalitesi:AVERAGE",
            "LINE1:havakalitesi#0000FF:Hava Kalitesi",
            "GPRINT:havakalitesi:MIN:Min Hava Kalitesi\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:havakalitesi:MAX:Max Hava Kalitesi\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
        if z== "monthly":
            period="m"
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"aylikSıcaklık.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Sicaklik(C)", "--height=300",
            "DEF:sicaklik="+dosyaKonumu+":sicaklik:AVERAGE",
            "DEF:dis_sicaklik="+dosyaKonumu+":dis_sicaklik:AVERAGE",
            "LINE1:sicaklik#00FF00:Oda Sicakligi",
            "LINE1:dis_sicaklik#FF0000:Dis Sicaklik",
            "COMMENT:\\n",
            "GPRINT:sicaklik:MIN:Min Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:sicaklik:MAX:Max Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",
            "GPRINT:dis_sicaklik:MIN:Min Dis Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:dis_sicaklik:MAX:Max Dis Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"aylikIşık_Şiddeti.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Isik Siddeti", "--height=300",
            "DEF:isik="+dosyaKonumu+":isik:AVERAGE",
            "LINE1:isik#FF0000:Isik Seviyesi",
            "GPRINT:isik:MIN:Min Isik\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:isik:MAX:Max Isik\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"aylikNem.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Nem(%)", "--height=300",
            "DEF:nemorani="+dosyaKonumu+":nemorani:AVERAGE",
            "LINE1:nemorani#000000:Nem Orani\\r",
            "GPRINT:nemorani:MIN:Min Nem Orani\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:nemorani:MAX:Max Nem Orani\: %6.2lf\\r",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"aylikHava_Kalitesi.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Hava Kalitesi", "--height=300",
            "DEF:havakalitesi="+dosyaKonumu+":havakalitesi:AVERAGE",
            "LINE1:havakalitesi#0000FF:Hava Kalitesi",
            "GPRINT:havakalitesi:MIN:Min Hava Kalitesi\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:havakalitesi:MAX:Max Hava Kalitesi\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
        if z== "yearly":
            period="y"
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"yillikSıcaklık.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Sicaklik(C)", "--height=300",
            "DEF:sicaklik="+dosyaKonumu+":sicaklik:AVERAGE",
            "DEF:dis_sicaklik="+dosyaKonumu+":dis_sicaklik:AVERAGE",
            "LINE1:sicaklik#00FF00:Oda Sicakligi",
            "LINE1:dis_sicaklik#FF0000:Dis Sicaklik",
            "COMMENT:\\n",
            "GPRINT:sicaklik:MIN:Min Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:sicaklik:MAX:Max Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",
            "GPRINT:dis_sicaklik:MIN:Min Dis Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:dis_sicaklik:MAX:Max Dis Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",             
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"yillikIşık_Şiddeti.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Isik Siddeti", "--height=300",
            "DEF:isik="+dosyaKonumu+":isik:AVERAGE",
            "LINE1:isik#FF0000:Isik Seviyesi",
            "GPRINT:isik:MIN:Min Isik\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:isik:MAX:Max Isik\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"yillikNem.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Nem(%)", "--height=300",
            "DEF:nemorani="+dosyaKonumu+":nemorani:AVERAGE",
            "LINE1:nemorani#000000:Nem Orani\\r",
            "GPRINT:nemorani:MIN:Min Nem Orani\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:nemorani:MAX:Max Nem Orani\: %6.2lf\\r",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../results/images/"+piid+"yillikHava_Kalitesi.png","--start","-1%s"%(period),"--width=800", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Hava Kalitesi", "--height=300",
            "DEF:havakalitesi="+dosyaKonumu+":havakalitesi:AVERAGE",
            "LINE1:havakalitesi#0000FF:Hava Kalitesi",
            "GPRINT:havakalitesi:MIN:Min Hava Kalitesi\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:havakalitesi:MAX:Max Hava Kalitesi\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)



