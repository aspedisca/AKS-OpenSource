#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""bu dosyaya rrd_update fonksiyonu yazılacak girdileri hostname(ip) dosya adı dosya knummu vs olacak.60sn de bir guncelleme işlemi gerçekleşecek buradan alınan verilerle son 30 dk daki 30 hareket verisinden yararlanılarak sistem için komut hesaplanacak.bu fonksiyon veri dinle.py dosyasının içinde çalışmaya baslayacak ve otomatik olarak guncelleme işlemi gelen verilerdden 60 sn de bir faydalanarak yapacak"""
import rrdtool
from rrdtool import update as rrd_update
def hareket_veritabani(dosyaKonumu):

    veritabani=rrdtool.create(dosyaKonumu,
    "--step","60","--start",'0',
    "DS:hareket:GAUGE:600:-1:2",
    "RRA:AVERAGE:0.5:2:120",
    "RRA:MAX:0.5:2:120",
    "RRA:MIN:0.5:2:120")
    #print(dosyaKonumu, "Sensor degerleri icin veritabani olusturuldu.")
    return veritabani

    
def hareket_veritabaniGuncelle(dosyaKonumu,hareket,dosyaAdi):           #veritabanlarini guncelleyen fonksiyon
    rrd_update(dosyaKonumu,'N:%s'%hareket); 

