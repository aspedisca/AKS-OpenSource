#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import os
import pickle as cPickle
"""
Bu uç birimleri kaldırmak için çalıştığı zaman veri tabanında kayıtlı olan uç birimlerin ve ip adreslerinin kaldırılmasını sağlıyor.
"""
class alan_kaldirma(QtGui.QMainWindow):
    def __init__(self, parent=None):
        """bu kısımda önce widgetlar oluşturulup daha sonra grid üzerine yerleştiriliyor."""
        super(alan_kaldirma, self).__init__(parent) 
        widget = QtGui.QWidget()
        self.setCentralWidget(widget)
        self.grid = QGridLayout()
        self.l1 = QtGui.QLabel("Uç birim kaldırma")
        self.l1.setAlignment(Qt.AlignCenter)
        self.Piip = QLabel('Uç birim IP : ')
        self.Piname = QLabel('Uç birim adı : ')        
        self.piip = QLineEdit()
        self.piname = QLineEdit()
        self.kaydet = QPushButton('Kaldır') 
        self.grid.addWidget(self.l1, 0, 0, 1, 2)
        self.grid.addWidget(self.Piname, 1, 0)
        self.grid.addWidget(self.piname, 1, 1)
        #self.grid.addWidget(self.Piip, 2, 0)
        #self.grid.addWidget(self.piip, 2, 1)
        self.grid.addWidget(self.kaydet, 5, 0, 1, 2) 
        self.connect(self.kaydet, SIGNAL('pressed()'), self.kaldirir)        
        widget.setLayout(self.grid)        
        self.setWindowTitle("Uç Birim Kaldırma")
        self.setMinimumSize(100,100)
        self.resize(200,200)
    def kaldirir(self):
        """bu fonksiyon veritabanından dosya okuyarak ekleme/cıkarma işlemlerini gerçekleştiriyor"""       
        piip = self.piip.text()
        piname= self.piname.text()
        print(piname)
        print(piip)
        datapi = open(os.getcwd()+"/../data/veritabani.db", 'rb')
        pilist=cPickle.load(datapi)
        print(pilist)
        pilist.pop(piname)
        print(pilist)
        datapi.close()
        datapi = open(os.getcwd()+"/../data/veritabani.db", 'wb')
        cPickle.dump(pilist, datapi)
        datapi.close()
