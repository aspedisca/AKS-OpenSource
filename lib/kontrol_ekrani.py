#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
from PyQt4.QtCore import * 
import os 
import pickle as cPickle
import sys 
from functools import partial 
import rrdtool    

"""
bu  class alan eklemek için kullanılıyor.eklenen uç birimleri veritabanına ekleyerek çalışıyor
"""
def alan_ekran(self,piname,hostname):
    #app = QApplication(sys.argv)
    #app = QApplication(sys.argv)
    win_kon = QWidget()

    anlik_baslik=QLabel(win_kon)
    anlik_baslik.setText(piname+" Anlik Değerleri")
    anlik_baslik.move(200,10)

    #burada rrd dosyasının son verisi 
    sonVeriler=rrdtool.lastupdate(os.getcwd()+"/../data/"+hostname+".rrd")
    sonGuncellemezamani=sonVeriler['date']
    sonSensorVerisi=sonVeriler['ds']
    
    sicaklik=sonVeriler['ds']['sicaklik']
    nemi=sonVeriler['ds']['nemorani']
    isiki=sonVeriler['ds']['isik']
    havak=sonVeriler['ds']['havakalitesi']
    
    sic=QLabel(win_kon)
    sic.setText("Sıcaklık:")
    sic.move(40,40)
    sicb=QLabel(win_kon)
    sicb.setText(str(sicaklik)+" C")
    sicb.move(80,40)  
    
    nem=QLabel(win_kon)
    nem.setText("Nem Oranı:")
    nem.move(130,40)

    nemb=QLabel(win_kon)
    nemb.setText("%"+str(nemi))
    nemb.move(190,40)

    isik=QLabel(win_kon)
    isik.setText("Işık Miktarı:")
    isik.move(240,40)

    isikb=QLabel(win_kon)
    isikb.setText(str(isiki))
    isikb.move(300,40)

    hava=QLabel(win_kon)
    hava.setText("Hava Kalitesi:")
    hava.move(350,40)    
    
    havab=QLabel(win_kon)
    havab.setText(str(havak))
    havab.move(420,40)

    tarihi=QLabel(win_kon)
    tarihi.setText("Son Güncelleme Zamanı:")
    tarihi.move(120,90)
    
    tarih=QLabel(win_kon)
    tarih.setText(str(sonGuncellemezamani))
    tarih.move(290,90)          
        
    grafik_baslik=QLabel(win_kon)
    grafik_baslik.setText(piname+" Kontrol Grafikleri")
    grafik_baslik.move(200,150)

    g = QLabel(win_kon)
    g.setText("Günlük")
    g.move(50,175)

    h = QLabel(win_kon)
    h.setText("Haftalık")
    h.move(150,175)

    a = QLabel(win_kon)
    a.setText("Aylık")
    a.move(250,175)

    y = QLabel(win_kon)
    y.setText("Yıllık")
    y.move(350,175)
    
    combog = QComboBox(win_kon)
    combog.addItems(["Grafik Türü","Sıcaklık", "Nem", "Hava_Kalitesi","Işık_Şiddeti"])
    combog.move(50,200)
    secileng=combog.currentText()
    self.connect(combog, QtCore.SIGNAL("currentIndexChanged(const QString&)"),partial(gunluk,secileng,hostname))
    
    comboh = QComboBox(win_kon)
    comboh.addItems(["Grafik Türü","Sıcaklık", "Nem", "Hava_Kalitesi","Işık_Şiddeti"])
    comboh.move(150,200)
    secilenh=comboh.currentText()
    self.connect(comboh, QtCore.SIGNAL("currentIndexChanged(const QString&)"),partial(haftalik,secilenh,hostname))    
    
    comboa = QComboBox(win_kon)
    comboa.addItems(["Grafik Türü","Sıcaklık", "Nem", "Hava_Kalitesi","Işık_Şiddeti"])
    comboa.move(250,200)
    secilena=comboa.currentText()
    self.connect(comboa, QtCore.SIGNAL("currentIndexChanged(const QString&)"),partial(aylik,secilena,hostname))    
    
    comboy = QComboBox(win_kon)
    comboy.addItems(["Grafik Türü","Sıcaklık", "Nem", "Hava_Kalitesi","Işık_Şiddeti"])
    comboy.move(350,200)
    secileny=comboy.currentText()
    self.connect(comboy, QtCore.SIGNAL("currentIndexChanged(const QString&)"),partial(yillik,secileny,hostname)) 
    
    konfor = QLabel(win_kon)
    konfor.setPixmap(QPixmap(os.getcwd()+"/../results/images/"+hostname+"konfor_bolgesi.png"))  
    konfor.move(1,220) 
    
    win_kon.setGeometry(750,750,520,700)
    win_kon.setWindowTitle(piname+" Kontrol Penceresi")
    win_kon.show()
    sys.exit(app.exec_())  
def gunluk(self,hostname,secileng):
    win = QDialog()
    l3 = QLabel()
    l3.setPixmap(QPixmap(os.getcwd()+"/../results/images/"+hostname+"günlük"+secileng+".png"))

    vbox = QVBoxLayout()
    vbox.addWidget(l3)
    win.setLayout(vbox)
    win.setWindowTitle("Günlük Grafik")
    win.show()
    sys.exit(app.exec_())
def haftalik(self,hostname,secilenh):
    win = QDialog()
    l5 = QLabel()
    l5.setPixmap(QPixmap(os.getcwd()+"/../results/images/"+hostname+"haftalik"+secilenh+".png"))

    vbox = QVBoxLayout()
    vbox.addWidget(l5)
    win.setLayout(vbox)
    win.setWindowTitle("Haftalık Grafik")
    win.show()
    sys.exit(app.exec_())
def aylik(self,hostname,secilena):
    win = QDialog()
    l6 = QLabel()
    l6.setPixmap(QPixmap(os.getcwd()+"/../results/images/"+hostname+"aylik"+secilena+".png"))

    vbox = QVBoxLayout()
    vbox.addWidget(l6)
    win.setLayout(vbox)
    win.setWindowTitle("Aylık Grafik")
    win.show()
    sys.exit(app.exec_())   
def yillik(self,hostname,secileny):
    win = QDialog()
    l7 = QLabel()
    l7.setPixmap(QPixmap(os.getcwd()+"/../results/images/"+hostname+"yillik"+secileny+".png"))

    vbox = QVBoxLayout()
    vbox.addWidget(l7)
    win.setLayout(vbox)
    win.setWindowTitle("Yıllık Grafik")
    win.show()
    sys.exit(app.exec_())    


        
