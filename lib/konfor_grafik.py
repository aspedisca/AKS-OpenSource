#!/usr/bin/env python
# -*- coding: utf-8 -*-
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib
import numpy as np
import os

def gr_konfor(isim,s,n):
    s=int(s)
    n=int(n)
    fig = plt.figure(figsize=(5.15,4.5))
    #ax = fig.gca(projection='3d')

    # Make data.
    X = np.arange(0, 100, 1)
    Y = np.arange(-30, 70, 1)
    #X, Y = np.meshgrid(X, Y)
    aspect=('auto')
    extent=(X.min(),X.max(),Y.min(),Y.max())
    Z=np.zeros([100,100])
    for i in np.arange(44,55):
        Z[i,:]=1
        Z[:,i]=1
    for j in np.arange(33,44):
        Z[j,:]=0.8
        Z[:,j]=0.8
    for j in np.arange(55,66):
        Z[j,:]=0.8
        Z[:,j]=0.8
    for j in np.arange(22,33):
        Z[j,:]=0.6
        Z[:,j]=0.6
    for j in np.arange(66,77):
        Z[j,:]=0.6
        Z[:,j]=0.6
    for j in np.arange(11,22):
        Z[j,:]=0.4
        Z[:,j]=0.4
    for j in np.arange(77,88):
        Z[j,:]=0.4
        Z[:,j]=0.4
    for j in np.arange(0,11):
        Z[j,:]=0.2
        Z[:,j]=0.2
    for j in np.arange(88,100):
        Z[j,:]=0.2
        Z[:,j]=0.2

    Z[s+30,n]=1.2
    Z[s+31,n]=1.2
    Z[s+30,n+1]=1.2
    Z[s+31,n+1]=1.2
    # Plot the surface.
    levels = [0, 0.2, 0.4, 0.6, 0.8, 1,1.2]
    im = plt.imshow(Z,cmap=cm.jet, origin="lower",extent=extent,aspect=aspect)
    CS3 = plt.contourf(X, Y, Z, levels,
                       colors=('#FFB300', '#EFFF00', '#96FF50','#54D200','#00A900','#FF0000'),
                       origin='lower',
                       extend='both')
    plt.title(' Konfor Bölgesi')
    plt.ylabel("Sıcaklık(C)")
    plt.xlabel("Nem(%)")
    matplotlib.rcParams.update({'font.size': 6})
    #print(os.getcwd())
    plt.savefig(os.getcwd()+"/../results/images/"+isim+"konfor_bolgesi.png",transparent=True)
    plt.close()
    #print("kaydettiii")

