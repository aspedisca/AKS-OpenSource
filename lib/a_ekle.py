#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
from PyQt4.QtCore import * 
import os 
import pickle as cPickle     

class alan_ekleme(QtGui.QMainWindow):
    def __init__(self, parent=None):

        super(alan_ekleme, self).__init__(parent) 
        widget = QtGui.QWidget()
        self.setCentralWidget(widget)
        self.grid = QGridLayout()
        self.onay=True
        self.l1 = QtGui.QLabel("Uç birim ekleme")
        self.l1.setAlignment(Qt.AlignCenter)
        self.Piip = QLabel('Uç birim Ip : ')
        self.Piname = QLabel('Uç birim adı: ')
        self.piip = QLineEdit()
        self.piname = QLineEdit()
        self.kaydet = QPushButton('Ekle') 
        self.grid.addWidget(self.l1, 0, 0, 1, 2)
        self.grid.addWidget(self.Piname, 1, 0)
        self.grid.addWidget(self.piname, 1, 1)
        self.grid.addWidget(self.Piip, 2, 0)
        self.grid.addWidget(self.piip, 2, 1)        
        self.grid.addWidget(self.kaydet, 5, 0, 1, 2) 
        self.connect(self.kaydet, SIGNAL('pressed()'), self.ekler)        
        widget.setLayout(self.grid)        
        self.setWindowTitle("Uç Birim Ekleme")
        self.setMinimumSize(200,200)
        self.resize(200,200)
    def sifre(self):
        widget = QtGui.QWidget()
        self.setCentralWidget(widget)
        self.grid = QGridLayout()
        self.siflabel=QLabel('Şifre  : ')
        self.sifedit=QLineEdit(self)
        self.tamam= QPushButton('Tamam') 
        self.grid.addWidget(self.siflabel, 0, 0)
        self.grid.addWidget(self.sifedit, 0, 1)
        self.grid.addWidget(self.tamam, 1,1)
        
        if (self.sifedit.text()=="aspire"):
            self.accept()
        else :
            
            QtGui.QMessageBox.warning(self, "Hay aksi","Geçersiz şifre")
        
        widget.setLayout(self.grid)        
        self.setWindowTitle("Yönetici Girişi")
        self.setMinimumSize(200,150)
        self.resize(200,150)
        
    def ekler(self):
        """bu fonksiyon veritabanından dosya okuyarak ekleme/cıkarma işlemlerini gerçekleştiriyor"""       
        piip = self.piip.text()
        piname= self.piname.text()
        print("piname"+piname)
        print("piip"+piip)
        datapi = open(os.getcwd()+"/../data/veritabani.db", 'rb')
        pilist=cPickle.load(datapi)
        print(pilist)
        pilist[piname]=piip
        print(pilist)
        datapi.close()
        datapi = open(os.getcwd()+"/../data/veritabani.db", 'wb')
        cPickle.dump(pilist, datapi)
        datapi.close()
