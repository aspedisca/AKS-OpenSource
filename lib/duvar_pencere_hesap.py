#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
def q_hesap(T_dis,T_ic):

    #kullanlan malzemelerin ısı iletkenlikleri
    k_duvar = 0.72
    k_yalitim = 0.040
    k_cam=0.78
    k_hava_cam = 0.78
    #havanın ışınım katsayısı
    h_ic_hava = 10
    h_dis_hava = 40

    #yüzey alanları
    alan_cam=1  #m2 cinsinden
    alan_duvar=6  #m2 cinsinden
    alan_yalitim=6  #m2 cinsinden

    #kalınlıklar

    l_cam=0.004 #metre cinsinden
    l_hava_cam=0.01 #metre cinsinden
    l_duvar=0.20   #metre cinsinden
    l_yalitim=0.03  #metre cinsinden

    #duvarda ısı transferi hesapları

    r1=1/(h_dis_hava*alan_yalitim)
    r2=l_yalitim/(k_yalitim*alan_yalitim)
    r3=l_duvar/(k_duvar*alan_duvar)
    r4=1/(h_ic_hava*alan_duvar)

    r_toplam=r1+r2+r3+r4

    Qduvar=(int(T_dis)-int(T_ic))/r_toplam
    Qduvar=np.abs(Qduvar)
    
    #camda ısı transferi hızları
    
    r1=1/(h_dis_hava*alan_cam)
    r2=l_cam/(k_cam*alan_cam)
    r3=l_hava_cam/(k_hava_cam*alan_cam)
    r4=l_cam/(k_cam*alan_cam)
    r5=1/(h_ic_hava*alan_cam)
    r_cam_toplam=r1+r2+r3+r4+r5
    
    Qcam=(int(T_dis)-int(T_ic))/r_cam_toplam
    Qcam=np.abs(Qcam)
    
    Qtoplam=Qduvar+Qcam
    
    #print("ısı transfer hızı = ",Qtoplam)
    return Qtoplam

