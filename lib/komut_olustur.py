#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rrdtool
import numpy as np
import time
import os
from duvar_pencere_hesap import *
from verim_test import *
from tuketim import *

"""veritabanındaki veriler kontrol edilerek 10 dk boyunca alınan verilerin ortalamasıyla komutlar olusturulacak."""
"""veri sırası sicaklık, ışık , hava kalitesi, nem oranı"""




def sicaklik_hesaplama(hostname):
    """son 10 dakikanın verilerinin ortalamasına göre eğer klima istemilen modda çalışmıyorsa klimanın modunu değiştirmek için yapılan hesaplamalar"""
    """ideal sıcaklık 20-23 C olarak düşünülmektedir."""
    veri_on_dakika=rrdtool.fetch(os.getcwd()+"/../data/"+hostname+".rrd","AVERAGE","-r","600","-s",str(int(time.time()-600)),"-e",str(int(time.time()-0)))
    sensor_verileri=veri_on_dakika[2]
    sicakliklar=[]
    #bu kısımda en sondaki veri okunup(dış sıcaklık verisi) Q değeri hesaplanacak daha sonra sıcaklık hesaplamaları bu Q değerinden faydalanılarak yapılacak.
    for veri_numarasi in np.arange(len(sensor_verileri)):
        sicaklik=(sensor_verileri[veri_numarasi][0])
        if isinstance(sicaklik,float):
            sicakliklar.append(sicaklik)
        else:
            pass
            #sicakliklar.append(np.nan)
    #bu kısma kadar yapılan sıcaklık okuma komutları iç ve dış sıcaklık için ayrı ayrı yapılacak         
    sicaklik_ortalamasi=np.mean(sicakliklar)
    
    veri_on_dakika=rrdtool.fetch(os.getcwd()+"/../data/"+hostname+".rrd","AVERAGE","-r","600","-s",str(int(time.time()-600)),"-e",str(int(time.time()-0)))
    sensor_verileri=veri_on_dakika[2]
    dis_sicakliklar=[]
    for veri_numarasi in np.arange(len(sensor_verileri)):
        dis_sicaklik=(sensor_verileri[veri_numarasi][4])
        if isinstance(dis_sicaklik,float):
            dis_sicakliklar.append(dis_sicaklik)
        else:
            pass
            #dis_sicakliklar.append(np.nan)
            

    dis_sicaklik_ort=np.mean(dis_sicakliklar)
    T_dis=dis_sicaklik_ort
    T_ic=sicaklik_ortalamasi
    #print(T_dis,T_ic)
    try:
        q=q_hesap(T_dis,T_ic)
    except:
        q=20
    #print(q)
    #eğer gereken işlem soğutma ise
    if T_ic>25 :
        if q>25 :
            komut_sicaklik=("fazlasogut")
        else:
            komut_sicaklik=("azsogut")
    #eğer gereken işlem ısıtma ise 
    elif T_ic<19 :
        if q>25 :
            komut_sicaklik=("fazlaisit")
        else:
            komut_sicaklik=("azisit") 
    else :
        if q>25 :
            if T_dis>25:
                komut_sicaklik=("azsogut")
            elif T_dis<19 :
                komut_sicaklik=("azisit")
            else:
                komut_sicaklik=("uygun")
        else:
            komut_sicaklik=("uygun")    
 
        
    return komut_sicaklik, T_dis
    
    

def hava_kalitesi_hesaplama(hostname):
    veri_on_dakika=rrdtool.fetch(os.getcwd()+"/../data/"+hostname+".rrd","AVERAGE","-r","600","-s",str(int(time.time()-600)),"-e",str(int(time.time()-0)))
    sensor_verileri=veri_on_dakika[2]
    hava_degerleri=[]
    for veri_numarasi in np.arange(len(sensor_verileri)):
        hava_kalitesi=(sensor_verileri[veri_numarasi][2])
        if isinstance(hava_kalitesi,float):
            hava_degerleri.append(hava_kalitesi)
        else:
            pass
            #hava_degerleri.append(np.nan)
            

    ortalama_hava_kalitesi=np.mean(hava_degerleri)
    #print(ortalama_hava_kalitesi)
    if ortalama_hava_kalitesi<=150:
        komut_hava="azfan"
    elif ortalama_hava_kalitesi>150 and ortalama_hava_kalitesi<=250:
        komut_hava="ortafan"
    else:
        komut_hava="fazlafan"
    return komut_hava





def isik_siddeti_hesaplama(hostname):
    veri_on_dakika=rrdtool.fetch(os.getcwd()+"/../data/"+hostname+".rrd","AVERAGE","-r","600","-s",str(int(time.time()-600)),"-e",str(int(time.time()-0)))
    sensor_verileri=veri_on_dakika[2]
    isik_degerleri=[]
    for veri_numarasi in np.arange(len(sensor_verileri)):
        isik_degeri=(sensor_verileri[veri_numarasi][1])
        if isinstance(isik_degeri,float):
            isik_degerleri.append(isik_degeri)
        else:
            pass
            #isik_degerleri.append(np.nan)
            

    ortalama_isik_siddeti=np.mean(isik_degerleri)
    #print("isik",ortalama_isik_siddeti)
    if ortalama_isik_siddeti <1000:
        belirlenen_isik_siddeti=0
    elif ortalama_isik_siddeti>=1000 and ortalama_isik_siddeti<1800:
        belirlenen_isik_siddeti=10
    elif ortalama_isik_siddeti>=1800 and ortalama_isik_siddeti<2900:
        belirlenen_isik_siddeti=20
    elif ortalama_isik_siddeti>=2900 and ortalama_isik_siddeti<3600:
        belirlenen_isik_siddeti=30
    elif ortalama_isik_siddeti>=3600 and ortalama_isik_siddeti<4500:
        belirlenen_isik_siddeti=40
    elif ortalama_isik_siddeti>=4500 and ortalama_isik_siddeti<5200:
        belirlenen_isik_siddeti=50
    elif ortalama_isik_siddeti>=5200 and ortalama_isik_siddeti<10000:
        belirlenen_isik_siddeti=60
    elif ortalama_isik_siddeti>=10000 and ortalama_isik_siddeti<17000:
        belirlenen_isik_siddeti=70
    elif ortalama_isik_siddeti>=17000 and ortalama_isik_siddeti<18500:
        belirlenen_isik_siddeti=80
    elif ortalama_isik_siddeti>=18500 and ortalama_isik_siddeti<20000:
        belirlenen_isik_siddeti=90
    elif ortalama_isik_siddeti>=20000:
        belirlenen_isik_siddeti=100
    else:
        belirlenen_isik_siddeti=0

    return belirlenen_isik_siddeti


        
def ortamda_hareket_hesaplama(hostname):
    """son 10 dakika içerisinde ortamda hareket varsa aydınlatma için açık komutu versin, ortamda hareket  yoksa aydınlatma için kapalı komutu versin"""
    veri_on_dakika=rrdtool.fetch(os.getcwd()+"/../data/hareket_verileri/"+hostname+"hareket.rrd","AVERAGE","-r","60","-s",str(int(time.time()-1920)),"-e",str(int(time.time()-120)))
    sensor_verileri=veri_on_dakika[2]
    hareket_sayac=0
    for veri_numarasi in np.arange(len(sensor_verileri)):
        hareket_verisi=(sensor_verileri[veri_numarasi][0])
        if isinstance(hareket_verisi,float):
            if hareket_verisi>0.02 and hareket_verisi<1.1:
                hareket_sayac=hareket_sayac+1
            else:
                pass
                
        else:
            pass
        if hareket_sayac >= 1:
            hareket_durumu=1
        else:
            hareket_durumu=0

    if hareket_durumu==1:
        komut_hareket="var"
    else:
        komut_hareket="yok"

    return komut_hareket

    
def komutlar(hostname):
    komut_hareket=ortamda_hareket_hesaplama(hostname)
    if komut_hareket == "yok" :
        klimamodu="uygun"
        komut_isik=0
        komutlar="uygun,0,uygun,azfan,yok"
    else :
        komut_sicaklik, dis_hava_sicaklik=sicaklik_hesaplama(hostname)
        #komut_nem=nem_hesaplama(hostname)
        komut_hava=hava_kalitesi_hesaplama(hostname)
        komut_isik=isik_siddeti_hesaplama(hostname)
        komut_hareket=ortamda_hareket_hesaplama(hostname)
        if komut_sicaklik == "azsogut" or komut_sicaklik == "fazlasogut":
            klimamodu="sogutmamodu"
            
        elif komut_sicaklik == "azisit" or komut_sicaklik == "fazlaisit":
            klimamodu="isitmamodu"
        elif komut_hava == "fazlafan" or komut_hava == "ortafan":
            klimamodu="fanmodu"
        else :
            klimamodu="uygun"
        komutlar=komut_sicaklik+","+str(komut_isik)+","+klimamodu+","+komut_hava+","+komut_hareket
        
        
    #Bu kısım test için yazıldı silinecek
    isik_durumu=int(komut_isik)
    klima_durumu=klimamodu

    lamba = (isik_durumu/100)*184
    if klima_durumu=="uygun":
        klima=0
    elif klima_durumu=="isitmamodu":
        klima=2000
    elif klima_durumu== "sogutmamodu":
        klima=1333
    elif klima_durumu=="fanmodu":
        klima=50
    else:
        klima=0
    try:
        ciz_test(klima,lamba)
    except:
        pass
    try:
        basla(komut_hareket)
    except:
        pass

    return komutlar
    
    
