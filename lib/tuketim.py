#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rrdtool
from rrdtool import update as rrd_update
import time
import datetime
import os




def veritabani(dosyaKonumu):
    veritabani=rrdtool.create(dosyaKonumu,
    "--step","300","--start",'0',
    "DS:aydinlatma:GAUGE:600:-1:2000",
    "DS:iklimlendirme:GAUGE:600:-1:2000",
    "RRA:AVERAGE:0.5:1:288",
    "RRA:MAX:0.5:1:288",
    "RRA:MIN:0.5:1:288")

    return veritabani
def verimveritabaniGuncelle(dosyaKonumu,aydinlatma,iklimlendirme,dosyaAdi):           #veritabanlarini guncelleyen fonksiyon
    rrd_update(dosyaKonumu,'N:%s:%s'%(aydinlatma,iklimlendirme));          #rrdtool kutuphanesindeki komut ile
    #print(dosyaAdi, "icin veritabanlari guncellendi.")
def verimgrafikCiz(dosyaKonumu):
    #piid=str(piid)
    sonGuncelleme = rrdtool.last(dosyaKonumu)
    sonGuncellemedf = datetime.datetime.fromtimestamp(int(sonGuncelleme)).strftime('%Y-%m-%d %H\:%M\:%S')
    for z in ["daily"]:
        if z== "daily":
            period="d"
            ciz=rrdtool.graph(os.getcwd()+"/../results/"+time.strftime("%d%m%Y")+"off.png","--start","-1%s"%(period),"--width=800", 
            "--title=Tuketim Grafigi(Aks Kapalı)","--watermark=ASPIRE LAB", "--vertical-label=Tuketim Seviyesi (Watt/saat)", "--height=300",
            "DEF:aydinlatma="+dosyaKonumu+":aydinlatma:AVERAGE",
            "DEF:iklimlendirme="+dosyaKonumu+":iklimlendirme:AVERAGE",
            "AREA:aydinlatma#0000FF:Aydinlatma",
            "AREA:iklimlendirme#00FF0080:Iklimlendirme",              
            "COMMENT:\\n",
            "COMMENT:  ",
            "GPRINT:aydinlatma:AVERAGE:Ortalama Aydinlatma Tuketimi\: %6.2lf\\r",
            "GPRINT:iklimlendirme:AVERAGE:Ortalama Iklimlendirme Tuketimi\: %6.2lf\\r",       
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
def ciz_testi(klima_calisma,isik_calisma):
    zaman=time.strftime("%H%M%S")
    if int(zaman[3]) == 1 or int(zaman[3]) == 4 or int(zaman[3]) == 8:
        verimdosyaAdi="tuketim.rrd"
        verimdosyaKonumu=os.getcwd()+"/../results/"+verimdosyaAdi
        if os.path.isfile(verimdosyaKonumu):
            pass
            #print( dosyaAdi,"icin veritabani daha once olusturulmus.")
        else:
            veritabani(verimdosyaKonumu)
            
        verimveritabaniGuncelle(verimdosyaKonumu,isik_calisma,klima_calisma,verimdosyaAdi)

        verimgrafikCiz(verimdosyaKonumu) 
    else:
        pass
    

    
    
    
def basla(hareket):
  
    zaman=time.strftime("%H%M%S")
    saat=zaman[0]+zaman[1]
    saat=int(saat)

    if hareket == "var":
        if saat < 8:
            klima=0
            aydinlatma=0
        elif saat >= 8 and saat < 10 :
            klima =1333
            aydinlatma=0
        elif saat >= 10 and saat < 12 :
            klima =1333
            aydinlatma=0
        elif saat >= 12 and saat < 13:
            klima=0
            aydinlatma =0
        elif saat >= 13 and saat < 15:
            klima=1333
            aydinlatma =0
            
        elif saat>=15 and saat <17 :
            klima=1333
            aydinlatma=184
        else :
            klima=0
            aydinlatma=0
    elif hareket == "yok":
        klima=0
        aydinlatma=0
    else :
        klima=0
        aydinlatma=0
    ciz_testi(klima,aydinlatma)
    
    

    

    
