#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import time
import os
import pickle as cPickle
from multiprocessing import Process
from grafik_olustur import *
from hareket_guncelle import *
from konfor_grafik import *
import subprocess
from alarm import *


def on_connect(client, userdata, flags, rc):
  #print("Connected with result code "+str(rc))
  client.subscribe("aks/veri")

def on_message(client, userdata, msg):
    gelen=msg.payload.decode()
    veri=gelen.split(",")
    isim=veri[0]
    sicaklik=float(veri[1])
    nem=float(veri[2])
    hava_kalitesi=int(veri[3])
    isik_siddeti=(float(veri[4]))
    hareket=int(veri[5])
    dis_sicaklik=float(veri[6])
    zaman=time.strftime("%H%M%S")
    saat=zaman[0]+zaman[1]
    saat=int(saat)
    if saat < 8 or saat >16 or saat==12:
        hareket =0
    else :
        pass
    if hava_kalitesi>100 :
        #pass
        try:
            acil_durum_ses()
        except ValueError and NameError and TypeError:
            pass
        #subprocess.call(["afplay", os.getcwd()+"/../data/emergency_alert.flac"])
    else:
        pass
    dosyaAdi=isim+".rrd"
    dosyaKonumu=os.getcwd()+"/../data/"+dosyaAdi
    hareket_dosyaAdi=isim+"hareket.rrd"
    hareket_dosyaKonumu=os.getcwd()+"/../data/hareket_verileri/"+hareket_dosyaAdi    
    if os.path.isfile(dosyaKonumu):
        pass
        #print( dosyaAdi,"icin veritabani daha once olusturulmus.")
    else:
        veritabani(dosyaKonumu)
    veritabaniGuncelle(dosyaKonumu,sicaklik,isik_siddeti,hava_kalitesi,nem,dis_sicaklik,dosyaAdi)
    grafikCiz(isim,dosyaKonumu)
    if os.path.isfile(hareket_dosyaKonumu):
        pass
        #print( dosyaAdi,"icin veritabani daha once olusturulmus.")
    else:
        hareket_veritabani(hareket_dosyaKonumu)
    hareket_veritabaniGuncelle(hareket_dosyaKonumu,hareket,hareket_dosyaAdi)
    print(hareket)
    try :
        gr_konfor(isim,sicaklik,nem)
    except:
        pass
    print(veri)
    client.disconnect()
    
    
def start(hostname):

       
    client = mqtt.Client()
    client.username_pw_set(username="aks",password="akssifre")
    client.connect(hostname,1883,60)
    client.on_connect = on_connect
    client.on_message = on_message
    """try:
        client.on_connect = on_connect
        client.on_message = on_message
    except:
        client.disconnect()
    client.loop_start()"""
    client.loop_forever()
    
def server():
    #print(os.getcwd()+"/../data/veritabani.db")
    while True:

        datapi = open(os.getcwd()+"/../data/veritabani.db", 'rb')
        pilist=cPickle.load(datapi)
        print(pilist)
        datapi.close()
        if len(pilist)>0:
            for name,hostname in sorted(pilist.items()): 
                start(hostname)
                #start("95.183.171.103")
        else:
            pass   
            
        time.sleep(30)

